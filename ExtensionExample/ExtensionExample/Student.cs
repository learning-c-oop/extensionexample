﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ExtensionExample
{
    public class Student : Person
    {
        public int Grade { get; set; }
    }
}
