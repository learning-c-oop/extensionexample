﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ExtensionExample
{
    public static class Extension
    {
        public static List<Student> Any(this List<Student> source, int minGrade, int maxGrade)
        {
            var result = new List<Student>();
            foreach (var student in source)
            {
                if (student.Grade > minGrade && student.Grade < maxGrade)
                {
                    result.Add(student);
                }
            }
            return result;
        }
        public static void BubbleSort(this List<Student> students)
        {
            for (int i = 0; i < students.Count - 1; i++)
            {
                for (int j = 0; j < students.Count - 1; j++)
                {
                    if (students[j].Grade > students[j + 1].Grade)
                    {
                        int temp = students[j].Grade;
                        students[j].Grade = students[j + 1].Grade;
                        students[j + 1].Grade = temp;
                    }
                }
            }
        }
    }
}
