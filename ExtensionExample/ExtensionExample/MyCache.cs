﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ExtensionExample
{
    public static class MyCache
    {
        static MyCache()
        {
            GetStudents(40);
        }
        public static void Init() { }
        public static List<Student> Students { get; private set; }
        private static void GetStudents(int count)
        {
            Students = new List<Student>(count);
            var rnd = new Random();
            for (int i = 0; i < count; i++)
            {
                var student = new Student
                {
                    Age = rnd.Next(15, 40),
                    Grade = rnd.Next(10, 100),
                    Name = $"A{i + 1}",
                    Surname = $"A{i + 1}yan"
                };
                Students.Add(student);
            }
        }
    }
}
