﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ExtensionExample
{
    class Program
    {
        static void Main(string[] args)
        {
            MyCache.Init();
            List<Student> students = MyCache.Students;
            Print(students);
            Console.WriteLine();
            students = students.Any(40, 100);
            Print(students);
            Console.WriteLine();
            students.BubbleSort();
            Print(students);

            Console.ReadKey();
        }
        static void Print(List<Student> students)
        {
            foreach (var student in students)
            {
                Console.WriteLine($"{student.Name} {student.Surname} {student.Age} {student.Grade}");
            }
        }
    }
}
